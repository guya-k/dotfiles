set ruler
set number
"set cmdheight=2

""" Mouse
if has('mouse')
  set mouse=a
endif

""" Searching
set hlsearch
set incsearch

""" Files
set encoding=utf8
set ffs=unix,dos,mac

""" Tabs
set expandtab
set smarttab

set shiftwidth=4
set tabstop=4

set ai
set si
set wrap
