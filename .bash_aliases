#!/bin/bash

alias quit="exit"


alias ssh-a="eval `ssh-agent`"

mvln () {
    mv $1 $2
    ln -s $2/$1 $1
}

activate () {
    if [ -d "$1" ] && [ -f "$1/bin/activate" ] ; then
        source "$1/bin/activate"
    fi
}

## vid coverimage audiofile(s)
vid () {
    mkdir -p vid
    
    # make things act right with spaces in filenames (x---d)
    SAVEIFS=$IFS
    IFS=$(echo -en "\n\b")
    
    for i in ${@:2}
    do
        echo "muxing cover \"$1\" and audio \"$i\"..."
        ~/ffmpeg-static/bin/ffmpeg -y -loop 1 -framerate 2 \
            -i "$1" -i "$i" \
            -c:v libx264 -preset slower -tune stillimage -crf 18 \
            -c:a copy \
            -shortest -pix_fmt yuv420p \
            vid/"$i".mp4
    done
    
    IFS=$SAVEIFS
}

#    ffmpeg -loop 1 -framerate 2 -i "$1" -i "$2" \
#        -c:v libx264 -preset slower -tune stillimage -crf 18 \
#        -c:a copy -shortest -pix_fmt yuv420p "$2".mp4
#        -c:a copy -strict experimental -b:a 192k -pix_fmt yuv420p \
    
#ffmpeg -i <input file> -codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart <output_name>.mp4